!-----------------------------------------------------------------
!
! provides basic MPI functionality
! implements a 'singleton' pattern
!
! author: Markus Rampp (RZG)
!
!-----------------------------------------------------------------
module my_mpi
#ifdef MPICODE
  use mpi
#endif
  private
  public :: initmpi

!-----------------------------------------------------------------
!
! basic MPI functionality
!
!-----------------------------------------------------------------
  type, public :: mpicomm
 
     integer :: mpicomm,myrank,numproc,info

   contains
     procedure, public :: finalize
     procedure, public :: isRoot
     procedure, public :: abort_io

  end type mpicomm



!members of my_mpi (globals)
  integer, public :: ierr
  type(mpicomm), pointer, public :: comm => null()


contains

!-----------------------------------------------------------------
  subroutine initmpi(ierr)
#ifdef MPICODE 
    use mpi
#endif
    implicit none


    integer, intent(out) :: ierr
 
    if (.not. associated(comm)) then 
       allocate(comm)
#ifdef MPICODE 
       comm%mpicomm=MPI_COMM_WORLD

       call MPI_INIT(ierr)
       call MPI_COMM_RANK(comm%mpicomm,comm%myrank,ierr)
       call MPI_COMM_SIZE(comm%mpicomm,comm%numproc,ierr)
#else
       comm%myrank  = 0
       comm%numproc = 1
       ierr = 0
#endif
    endif

  end subroutine initmpi
  
!-----------------------------------------------------------------
  subroutine finalize(this)
#ifdef MPICODE 
    use mpi
#endif
    implicit none

    class(mpicomm) :: this 

    integer ierr
#ifdef MPICODE
    call MPI_FINALIZE(ierr)
#endif
  end subroutine finalize

!-----------------------------------------------------------------
  logical function isRoot(this)

    implicit none

    class(mpicomm) :: this 
    isRoot = (this%myrank==0)

    return 

  end function isRoot
!-----------------------------------------------------------------  
!> abort_io
  subroutine abort_io(this,ioerror,message)
#ifdef MPICODE 
    use mpi
#endif
    implicit none
    
    class(mpicomm) :: this 
    integer, intent(in) :: ioerror
    character(*), intent(in) :: message

    if (ioerror .lt. 0) then
      print *,ioerror,message

#if defined(MPICODE) 
      call MPI_FINALIZE(ierr)
#endif
      stop 'panic'
   endif
   
  end subroutine abort_io

end module my_mpi
