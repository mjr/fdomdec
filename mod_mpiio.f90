module mod_mpiio 
  use precision
  use mpivar

  implicit none
  public :: create_file_mpiio, close_file_mpiio

  INTEGER :: mpi_comm, mpi_info, ierr, mpi_size, mpi_rank

  integer :: methodMPI

  type :: datafileMPI
    integer                       :: id
    character(LEN=:), allocatable :: name     !name = base+suffix
    character(LEN=:), allocatable :: base
    character(LEN=:), allocatable :: suffix
  end type

  integer :: error

contains

  subroutine create_file_mpiio(basename,suffix,file)
    use mpivar
    implicit none
    type(datafileMPI), intent(out) :: file
    character(*), intent(in) :: basename
    character(*), intent(in) :: suffix

    allocate(character(LEN=len_trim(basename)) :: file%base)
    file%base = trim(basename)
    allocate(character(LEN=len_trim(suffix)) :: file%suffix)
    file%suffix = trim(suffix)
    allocate(character(LEN=len_trim(basename)+len_trim(suffix)) :: file%name)
    file%name = file%base//file%suffix
     
    call MPI_FILE_OPEN(MPI_COMM_WORLD, file%name, MPI_MODE_WRONLY + MPI_MODE_CREATE, & 
                       info, file%id, ierror) 

    call abort_io(ierror,"ERROR in create_file_mpiio")

    if (master()) write(*,*) " "
    if (master()) write(*,*) "Created the MPIIO file with name: ", file%name

  end subroutine create_file_mpiio

  !> Close an opened file

  subroutine close_file_mpiio(file)
    use mpivar
    implicit none
    type(datafileMPI), intent(in) :: file
    integer :: error

    call MPI_FILE_CLOSE(file%id,ierror)
    call abort_io(ierror,"ERROR in close_file_mpiio")
    if (master()) write(*,*), "Closed the file: ",file%name

  end subroutine close_file_mpiio


!> Start the mpiio test for 1d array distrubuted among the processes
!> usind 1d cartesian grid
subroutine start_test_1dcart_1darraympiio(Nx, mpiiofile)
  use mytiming
  use mpivar

  implicit none
  integer(kind=ik), intent(in) :: Nx
  type(datafileMPI)            :: mpiiofile
  real(kind=rk), allocatable, dimension(:)   :: A
  integer(kind=ik) :: xs, xe
  integer :: localSize, localSizeMAX, localSizeMIN !in MB
  integer(kind=ik), parameter :: LIMIT_2G = (1024)*2 !in MB

  integer :: commCart			!Cartesian communicator
  integer(kind=ik) :: i
  integer(kind=ik) :: xinner1
  integer, parameter  :: cartDim = 1
  integer :: dimsCart(1), coords(1)		!number of nodes in each dimension
  integer(kind=ik) :: xcoord	        !Local coordinates
  integer(kind=ik) :: xinner, xouter, xin
  logical :: periods(1), reorder	!Periods of the cartesian grid and reorder
 
  integer :: subarray, status(MPI_STATUS_SIZE)
  integer :: gsize(1), lsize(1), starts(1)

  methodMPI = 1

  periods(1)  = .false.
  reorder     = .false.
  
  call MPI_DIMS_CREATE(nprocs, cartDim, dimsCart, ierror)
  call MPI_CART_CREATE(MPI_COMM_WORLD, cartDim, dimsCart, periods, reorder, commCart, ierror)
  call MPI_COMM_RANK(commCart, rank, ierror)
  call MPI_CART_COORDS(commCart, rank, cartDim ,coords, ierror)

  xcoord = coords(1)

 if(master()) then
  write(*,*), "Test the 1D array in a 1D cartesian grid"
  write(*,*), "Total Number of procs:   ",nprocs
  write(*,*), "Number of procs along x: ",dimsCart(1)
  write(*,'(A,I10)'), " Array dimensions:",Nx
 endif

  xinner1 = (Nx - 1)/ dimsCart(1) + 1
  xin     =  Nx - dimsCart(1)*(xinner1 - 1)
  if(xcoord .lt. xin) then
	xinner = xinner1
	xs = 1 + xcoord*xinner
  else
	xinner = xinner1 - 1
	xs = 1 + xin*xinner1 + (xcoord - xin) * xinner
  endif
  xouter = xinner 
  xe = xs + xouter - 1	

  allocate(A(xs:xe))
  localSize = sizeof(A)/(1024*1024)

#if defined(DEBUG)
 do i=0,nprocs-1
  if(rank.eq.i) then
  write(*,*) 'PE = ', rank,' xs = ', xs, ' xe = ', xe 
  write(*,*) 'Local Size[MB] = ', localSize	
  write(*,*)
  endif
  call MPI_BARRIER(MPI_COMM_WORLD,ierror)
enddo
#endif

  do i = xs, xe
    A(i) = (real(i, kind=rk))
  enddo

  gsize(1)  = Nx
  lsize(1)  = xe-xs+1
  starts(1) = xs-1							

  call MPI_TYPE_CREATE_SUBARRAY(1, gsize, lsize, starts, MPI_ORDER_FORTRAN, MPI_DOUBLE_PRECISION, subarray, ierror)
  call abort_io(ierror,"ERROR in MPI_TYPE_CREATE_SUBARRAY")
  call MPI_TYPE_COMMIT(subarray, ierror)
  call abort_io(ierror,"ERROR in MPI_TYPE_COMMIT")
  call MPI_FILE_SET_VIEW(mpiiofile%id, 0, MPI_DOUBLE_PRECISION, subarray, "native", MPI_INFO_NULL, ierror)
  call abort_io(ierror,"ERROR in MPI_FILE_SET_VIEW")

  call MPI_Reduce(localSize, localSizeMAX, 1, MPI_INTEGER, MPI_MAX, 0, MPI_COMM_WORLD, ierror)
  call MPI_Reduce(localSize, localSizeMIN, 1, MPI_INTEGER, MPI_MIN, 0, MPI_COMM_WORLD, ierror)
  call MPI_Bcast(localSizeMAX, 1, MPI_INTEGER, 0,MPI_COMM_WORLD,ierror)

if(master()) then
   write(*,'(A,I10)'), "Local Dim A(x)      = ", xe-xs+1
   write(*,'(A,I10)'), "Local size MAX [MB] = ", localSizeMAX
   write(*,'(A,I10)'), "Local size MIN [MB] = ", localSizeMIN
endif

if(localSizeMAX .lt. LIMIT_2G) then
  call MPI_BARRIER(MPI_COMM_WORLD,ierror)
  write_time = takeTime()
  call MPI_FILE_WRITE_ALL(mpiiofile%id, A, lsize, MPI_DOUBLE_PRECISION, status, ierror)
  call abort_io(ierror,"ERROR in MPI_FILE_WRITE_ALL")
  call MPI_BARRIER(MPI_COMM_WORLD,ierror)
  write_time = takeTime() - write_time
else
  if(master()) then
	write(*,*), "** The Local Array of rank ", rank, "has dimension: ",localSize
        write(*,*), "** which is larger than ",LIMIT_2G
  	write(*,*), "** Test the 1D array in a 1D cartesian grid: FAILED"
  endif
endif

  call MPI_TYPE_FREE(subarray,ierror) 
  call MPI_COMM_FREE(commCart, ierror) 
  deallocate(A)

end subroutine start_test_1dcart_1darraympiio


!> Start the mpiio test for 2d array distrubuted among the processes
!> usind 1d cartesian grid
subroutine start_test_1dcart_2darraympiio(Nx, Ny, mpiiofile)
  use mytiming
  use mpivar

  implicit none
  integer(kind=ik), intent(in) :: Nx, Ny
  type(datafileMPI)            :: mpiiofile
  real(kind=rk), allocatable, dimension(:,:)   :: A

  integer(kind=ik) :: xs, xe
  integer(kind=ik) :: ys, ye
  integer :: localSize, localSizeMAX, localSizeMIN  !in MB
  integer(kind=ik), parameter :: LIMIT_2G = (1024)*2 !in MB

  integer :: commCart			!Cartesian communicator
  integer(kind=ik) :: i,j
  integer(kind=ik) :: xinner1
  integer, parameter  :: cartDim = 1
  integer :: dimsCart(1), coords(1)		!number of nodes in each dimension
  integer(kind=ik) :: xcoord	        !Local coordinates
  integer(kind=ik) :: xinner, xouter, xin
  logical :: periods(1), reorder	!Periods of the cartesian grid and reorder

  integer :: subarray, status(MPI_STATUS_SIZE)
  integer :: gsize(2), lsize(2), starts(2)

  methodMPI = 2

  periods(1)  = .false.
  reorder     = .false.
  
  call MPI_DIMS_CREATE(nprocs, cartDim, dimsCart, ierror)
  call MPI_CART_CREATE(MPI_COMM_WORLD, cartDim, dimsCart, periods, reorder, commCart, ierror)
  call MPI_COMM_RANK(commCart, rank, ierror)
  call MPI_CART_COORDS(commCart, rank, cartDim ,coords, ierror)

  xcoord = coords(1)

 if(master()) then
  write(*,*), "Test the 2D array in a 1D cartesian grid"
  write(*,*), "Total Number of procs:   ",nprocs
  write(*,*), "Number of procs along x: ",dimsCart(1)
  write(*,'(A,2I10)'), " Array dimensions:",Nx,Ny
 endif

  xinner1 = (Nx - 1)/ dimsCart(1) + 1
  xin     =  Nx - dimsCart(1)*(xinner1 - 1)
  if(xcoord .lt. xin) then
	xinner = xinner1
	xs = 1 + xcoord*xinner
  else
	xinner = xinner1 - 1
	xs = 1 + xin*xinner1 + (xcoord - xin) * xinner
  endif
  xouter = xinner 
  xe = xs + xouter - 1	

  ys = 1
  ye = Ny

  allocate(A(ys:ye,xs:xe))
  localSize = sizeof(A)/(1024*1024)

#if defined(DEBUG)
 do i=0,nprocs-1
  if(rank.eq.i) then
  write(*,*) 'PE = ', rank,' xs = ', xs, ' xe = ', xe 	
  write(*,*) 'PE = ', rank,' ys = ', ys, ' ye = ', ye 
  write(*,*) 'Local Size[MB] = ', localSize	
  write(*,*)
  endif
  call MPI_BARRIER(MPI_COMM_WORLD,ierror)
enddo
#endif


  do i = xs, xe
   do j = ys, ye
    A(j,i) = (ye-ys+1)*(i-1) + j
   enddo
  enddo

! do i=0,nprocs-1
!  if(rank.eq.i) then
!  write(*,*) 'PE = ', rank, A(:,:)	
!  
!  write(*,*)
!  endif
!  call MPI_BARRIER(MPI_COMM_WORLD,ierror)
!enddo
  

  gsize(1) = Ny
  gsize(2) = Nx
  lsize(1) = ye-ys+1
  lsize(2) = xe-xs+1
  starts(1)= ys-1
  starts(2)= xs-1

  call MPI_TYPE_CREATE_SUBARRAY(2, gsize, lsize, starts, MPI_ORDER_FORTRAN, MPI_DOUBLE_PRECISION, subarray, ierror)
  call abort_io(ierror,"ERROR in MPI_TYPE_CREATE_SUBARRAY")
  call MPI_TYPE_COMMIT(subarray, ierror)
  call abort_io(ierror,"ERROR in MPI_TYPE_COMMIT")
  call MPI_FILE_SET_VIEW(mpiiofile%id, 0, MPI_DOUBLE_PRECISION, subarray, "native", MPI_INFO_NULL, ierror)
  call abort_io(ierror,"ERROR in MPI_FILE_SET_VIEW")

  call MPI_Reduce(localSize, localSizeMAX, 1, MPI_INTEGER, MPI_MAX, 0, MPI_COMM_WORLD, ierror)
  call MPI_Reduce(localSize, localSizeMIN, 1, MPI_INTEGER, MPI_MIN, 0, MPI_COMM_WORLD, ierror)
  call MPI_Bcast(localSizeMAX, 1, MPI_INTEGER, 0,MPI_COMM_WORLD,ierror)

if(master()) then
   write(*,'(A,2I10)'), " Local Dim A(y,x) = ", ye-ys+1, xe-xs+1
   write(*,*), "Local size MAX [MB] = ", localSizeMAX
   write(*,*), "Local size MIN [MB] = ", localSizeMIN
endif

if(localSizeMAX .lt. LIMIT_2G) then
  call MPI_BARRIER(MPI_COMM_WORLD,ierror)
  write_time = takeTime()
  call MPI_FILE_WRITE_ALL(mpiiofile%id, A, lsize(1)*lsize(2), MPI_DOUBLE_PRECISION, status, ierror)
  call abort_io(ierror,"ERROR in MPI_FILE_WRITE_ALL")
  call MPI_BARRIER(MPI_COMM_WORLD,ierror)
  write_time = takeTime() - write_time
else
  if(master()) then
	write(*,*), "** The Local Array of rank ", rank, "has dimension: ",localSize
        write(*,*), "** which is larger than ",LIMIT_2G
  	write(*,*), "** Test the 2D array in a 1D cartesian grid: FAILED"
  endif
endif
  
  call MPI_TYPE_FREE(subarray,ierror) 
  call MPI_COMM_FREE(commCart, ierror) 
  deallocate(A)

end subroutine  start_test_1dcart_2darraympiio


!> Start the mpiio test for 3d array distrubuted among the processes
!> usind 1d cartesian grid
subroutine start_test_1dcart_3darraympiio(Nx, Ny,Nz, mpiiofile)
  use mytiming
  use mpivar

  implicit none
  integer(kind=ik), intent(in) :: Nx, Ny, Nz
  type(datafileMPI)            :: mpiiofile
  real(kind=rk), allocatable, dimension(:,:,:)   :: A

  integer(kind=ik) :: xs, xe
  integer(kind=ik) :: ys, ye
  integer(kind=ik) :: zs, ze

  integer :: localSize, localSizeMAX, localSizeMIN  !in MB
  integer(kind=ik), parameter :: LIMIT_2G = (1024)*2 !in MB

  integer :: commCart			!Cartesian communicator
  integer(kind=ik) :: i,j,k
  integer(kind=ik) :: xinner1
  integer, parameter  :: cartDim = 1
  integer :: dimsCart(1), coords(1)		!number of nodes in each dimension
  integer(kind=ik) :: xcoord	        !Local coordinates
  integer(kind=ik) :: xinner, xouter, xin
  logical :: periods(1), reorder	!Periods of the cartesian grid and reorder

  integer :: subarray, status(MPI_STATUS_SIZE)
  integer :: gsize(3), lsize(3), starts(3)

  methodMPI = 3

  periods(1)  = .false.
  reorder     = .false.
  
  call MPI_DIMS_CREATE(nprocs, cartDim, dimsCart, ierror)
  call MPI_CART_CREATE(MPI_COMM_WORLD, cartDim, dimsCart, periods, reorder, commCart, ierror)
  call MPI_COMM_RANK(commCart, rank, ierror)
  call MPI_CART_COORDS(commCart, rank, cartDim ,coords, ierror)

  xcoord = coords(1)

 if(master()) then
  write(*,*), "Test the 3D array in a 1D cartesian grid"
  write(*,*), "Total Number of procs:   ",nprocs
  write(*,*), "Number of procs along x: ",dimsCart(1)
  write(*,'(A,3I10)'), " Array dimensions:",Nx,Ny,Nz
 endif

  xinner1 = (Nx - 1)/ dimsCart(1) + 1
  xin     =  Nx - dimsCart(1)*(xinner1 - 1)
  if(xcoord .lt. xin) then
	xinner = xinner1
	xs = 1 + xcoord*xinner
  else
	xinner = xinner1 - 1
	xs = 1 + xin*xinner1 + (xcoord - xin) * xinner
  endif
  xouter = xinner 
  xe = xs + xouter - 1	

  ys = 1
  ye = Ny

  zs = 1
  ze = Nz
  
  allocate(A(zs:ze,ys:ye,xs:xe))
  localSize = sizeof(A)/(1024*1024) 


#if defined(DEBUG)
 do i=0,nprocs-1
  if(rank.eq.i) then
  write(*,*) 'PE = ', rank,' xs = ', xs, ' xe = ', xe 	
  write(*,*) 'PE = ', rank,' ys = ', ys, ' ye = ', ye 
  write(*,*) 'PE = ', rank,' zs = ', zs, ' ze = ', ze
  write(*,*) 'Local Size[MB] = ', localSize	
  write(*,*)
  endif
  call MPI_BARRIER(MPI_COMM_WORLD,ierror)
enddo
#endif

  call MPI_BARRIER(MPI_COMM_WORLD,ierror)
  do i = xs, xe
   do j = ys, ye
    do k = zs, ze
      A(k,j,i) = real(k + (ze-zs+1)*((ye-ys+1)*(i-1) + j-1),kind=rk)
    enddo
   enddo
  enddo
  call MPI_BARRIER(MPI_COMM_WORLD,ierror)

  gsize(1) = Nz
  gsize(2) = Ny
  gsize(3) = Nx
  lsize(1) = ze-zs+1
  lsize(2) = ye-ys+1
  lsize(3) = xe-xs+1
  starts(1)= zs-1
  starts(2)= ys-1
  starts(3)= xs-1

  call MPI_TYPE_CREATE_SUBARRAY(3, gsize, lsize, starts, MPI_ORDER_FORTRAN, MPI_DOUBLE_PRECISION, subarray, ierror)
  call abort_io(ierror,"ERROR in MPI_TYPE_CREATE_SUBARRAY")
  call MPI_TYPE_COMMIT(subarray, ierror)
  call abort_io(ierror,"ERROR in MPI_TYPE_COMMIT")
  call MPI_FILE_SET_VIEW(mpiiofile%id, 0, MPI_DOUBLE_PRECISION, subarray, "native", MPI_INFO_NULL, ierror)
  call abort_io(ierror,"ERROR in MPI_FILE_SET_VIEW")

  call MPI_Reduce(localSize, localSizeMAX, 1, MPI_INTEGER, MPI_MAX, 0, MPI_COMM_WORLD, ierror)
  call MPI_Reduce(localSize, localSizeMIN, 1, MPI_INTEGER, MPI_MIN, 0, MPI_COMM_WORLD, ierror)
  call MPI_Bcast(localSizeMAX, 1, MPI_INTEGER, 0,MPI_COMM_WORLD,ierror)

if(master()) then
   write(*,'(A,3I10)'), " Local Dim A(z,y,x)  = ", ze-zs+1,ye-ys+1, xe-xs+1
   write(*,*), "Local size MAX [MB] = ", localSizeMAX
   write(*,*), "Local size MIN [MB] = ", localSizeMIN
endif

if(localSizeMAX .lt. LIMIT_2G) then
  call MPI_BARRIER(MPI_COMM_WORLD,ierror)
  write_time = takeTime()
  call MPI_FILE_WRITE_ALL(mpiiofile%id, A, lsize(1)*lsize(2)*lsize(3), MPI_DOUBLE_PRECISION, status, ierror)
  call abort_io(ierror,"ERROR in MPI_FILE_WRITE_ALL")
  call MPI_BARRIER(MPI_COMM_WORLD,ierror)
  write_time = takeTime() - write_time
else
  if(master()) then
	write(*,*), "** The Local Array of rank ", rank, "has dimension: ",localSize
        write(*,*), "** which is larger than ",LIMIT_2G
  	write(*,*), "** Test the 3D array in a 1D cartesian grid: FAILED"
  endif
endif
  
  call MPI_TYPE_FREE(subarray,ierror) 
  call MPI_COMM_FREE(commCart, ierror) 
  deallocate(A)

end subroutine  start_test_1dcart_3darraympiio

subroutine start_test_2dcart_2darraympiio(Nx, Ny, mpiiofile)
  use mpivar
  use mytiming

  implicit none
  integer(kind=ik), intent(in) :: Nx, Ny
  type(datafileMPI)      :: mpiiofile
  real(kind=rk), allocatable, dimension(:,:)   :: A

  integer(kind=ik) :: xs, xe
  integer(kind=ik) :: ys, ye
  integer :: localSize, localSizeMAX, localSizeMIN  !in MB
  integer(kind=ik), parameter :: LIMIT_2G = (1024)*2 !in MB

  integer :: commCart			!Cartesian communicator
  integer(kind=ik) :: i,j
  integer(kind=ik) :: xinner1, yinner1
  integer, parameter  :: cartDim = 2
  integer :: dimsCart(2), coords(2)		!number of nodes in each dimension
  integer(kind=ik) :: xcoord, ycoord	!Local coordinates
  integer(kind=ik) :: xinner, xouter, xin
  integer(kind=ik) :: yinner, youter, yin
  logical :: periods(2), reorder	!Periods of the cartesian grid and reorder

  integer :: subarray, status(MPI_STATUS_SIZE)
  integer :: gsize(2), lsize(2), starts(2)
 
  methodMPI = 4

  periods(1)  = .false.
  periods(2)  = .false.
  reorder     = .false.
  
  call MPI_DIMS_CREATE(nprocs, cartDim, dimsCart, ierror)
  call MPI_CART_CREATE(MPI_COMM_WORLD, cartDim, dimsCart, periods, reorder, commCart, ierror)
  call MPI_COMM_RANK(commCart, rank, ierror)
  call MPI_CART_COORDS(commCart, rank, cartDim ,coords, ierror)

  xcoord = coords(1)
  ycoord = coords(2)

 if(master()) then
  write(*,*), "Test the 2D array in a 2D cartesian grid"
  write(*,*), "Total Number of procs:   ",nprocs
  write(*,*), "Number of procs along x: ",dimsCart(1)
  write(*,*), "Number of procs along y: ",dimsCart(2)
  write(*,'(A,2I10)'), " Array dimensions:",Nx,Ny
 endif

  xinner1 = (Nx - 1)/ dimsCart(1) + 1
  xin     =  Nx - dimsCart(1)*(xinner1 - 1)
  if(xcoord .lt. xin) then
	xinner = xinner1
	xs = 1 + xcoord*xinner
  else
	xinner = xinner1 - 1
	xs = 1 + xin*xinner1 + (xcoord - xin) * xinner
  endif
  xouter = xinner 
  xe = xs + xouter - 1	
!!  
  yinner1 = (Ny - 1)/ dimsCart(2) + 1
  yin     =  Ny - dimsCart(2)*(yinner1 - 1)
  if(ycoord .lt. yin) then
	yinner = yinner1
	ys = 1 + ycoord*yinner
  else
	yinner = yinner1 - 1
	ys = 1 + yin*yinner1 + (ycoord - yin) * yinner
  endif
  youter = yinner 
  ye = ys + youter - 1	

  allocate(A(ys:ye,xs:xe))
  localSize = sizeof(A)/(1024*1024) 

#if defined(DEBUG)
 do i=0,nprocs-1
  if(rank.eq.i) then
  write(*,*) 'PE = ', rank,' xs = ', xs, ' xe = ', xe 	
  write(*,*) 'PE = ', rank,' ys = ', ys, ' ye = ', ye 	
  write(*,*) 'Local Size[MB] = ', localSize	
  write(*,*)
  endif
  call MPI_BARRIER(MPI_COMM_WORLD,ierror)
enddo
#endif


  do i = xs, xe
   do j = ys, ye
    A(j,i) = (Ny)*(i-1) + j
   enddo
  enddo
  
  gsize(1) = Ny
  gsize(2) = Nx
  lsize(1) = ye-ys+1
  lsize(2) = xe-xs+1
  starts(1)= ys-1
  starts(2)= xs-1

  call MPI_TYPE_CREATE_SUBARRAY(2, gsize, lsize, starts, MPI_ORDER_FORTRAN, MPI_DOUBLE_PRECISION, subarray, ierror)
  call abort_io(ierror,"ERROR in MPI_TYPE_CREATE_SUBARRAY")
  call MPI_TYPE_COMMIT(subarray, ierror)
  call abort_io(ierror,"ERROR in MPI_TYPE_COMMIT")
  call MPI_FILE_SET_VIEW(mpiiofile%id, 0, MPI_DOUBLE_PRECISION, subarray, "native", MPI_INFO_NULL, ierror)
  call abort_io(ierror,"ERROR in MPI_FILE_SET_VIEW")

call MPI_Reduce(localSize, localSizeMAX, 1, MPI_INTEGER, MPI_MAX, 0, MPI_COMM_WORLD, ierror)
call MPI_Reduce(localSize, localSizeMIN, 1, MPI_INTEGER, MPI_MIN, 0, MPI_COMM_WORLD, ierror)
call MPI_Bcast(localSizeMAX, 1, MPI_INTEGER, 0,MPI_COMM_WORLD,ierror)

if(master()) then
   write(*,'(A,2I10)'), " Local Dim A(y,x)      = ", ye-ys+1, xe-xs+1
   write(*,*), "Local size MAX [MB] = ", localSizeMAX
   write(*,*), "Local size MIN [MB] = ", localSizeMIN
endif

if(localSizeMAX .lt. LIMIT_2G) then
  call MPI_BARRIER(MPI_COMM_WORLD,ierror)
  write_time = takeTime()
  call MPI_FILE_WRITE_ALL(mpiiofile%id, A, lsize(1)*lsize(2), MPI_DOUBLE_PRECISION, status, ierror)
  call abort_io(ierror,"ERROR in MPI_FILE_WRITE_ALL")
  call MPI_BARRIER(MPI_COMM_WORLD,ierror)
  write_time = takeTime() - write_time
else
  if(master()) then
	write(*,*), "** The Local Array of rank ", rank, "has dimension: ",localSize
        write(*,*), "** which is larger than ",LIMIT_2G
  	write(*,*), "** Test the 2D array in a 2D cartesian grid: FAILED"
  endif
endif
 
  call MPI_TYPE_FREE(subarray,ierror) 
  call MPI_COMM_FREE(commCart, ierror) 
  deallocate(A)

end subroutine start_test_2dcart_2darraympiio

subroutine start_test_2dcart_3darraympiio(Nx, Ny,Nz, mpiiofile)
  use mpivar
  use mytiming

  implicit none
  integer(kind=ik), intent(in) :: Nx, Ny, Nz
  type(datafileMPI)      :: mpiiofile
  real(kind=rk), allocatable, dimension(:,:,:)   :: A

  integer(kind=ik) :: xs, xe
  integer(kind=ik) :: ys, ye
  integer(kind=ik) :: zs, ze
  integer :: localSize, localSizeMAX, localSizeMIN  !in MB
  integer(kind=ik), parameter :: LIMIT_2G = (1024)*2 !in MB

  integer :: commCart			!Cartesian communicator
  integer(kind=ik) :: i,j,k
  integer(kind=ik) :: xinner1, yinner1
  integer, parameter  :: cartDim = 2
  integer :: dimsCart(2), coords(2)		!number of nodes in each dimension
  integer(kind=ik) :: xcoord, ycoord	!Local coordinates
  integer(kind=ik) :: xinner, xouter, xin
  integer(kind=ik) :: yinner, youter, yin
  logical :: periods(2), reorder	!Periods of the cartesian grid and reorder

  integer :: subarray, status(MPI_STATUS_SIZE)
  integer :: gsize(3), lsize(3), starts(3)
 
  methodMPI = 5

  periods(1)  = .false.
  periods(2)  = .false.
  reorder     = .false.
  
  call MPI_DIMS_CREATE(nprocs, cartDim, dimsCart, ierror)
  call MPI_CART_CREATE(MPI_COMM_WORLD, cartDim, dimsCart, periods, reorder, commCart, ierror)
  call MPI_COMM_RANK(commCart, rank, ierror)
  call MPI_CART_COORDS(commCart, rank, cartDim ,coords, ierror)

  xcoord = coords(1)
  ycoord = coords(2)

 if(master()) then
  write(*,*), "Test the 3D array in a 2D cartesian grid"
  write(*,*), "Total Number of procs:   ",nprocs
  write(*,*), "Number of procs along x: ",dimsCart(1)
  write(*,*), "Number of procs along y: ",dimsCart(2)
  write(*,'(A,3I10)'), " Array dimensions:",Nx,Ny,Nz
 endif

  xinner1 = (Nx - 1)/ dimsCart(1) + 1
  xin     =  Nx - dimsCart(1)*(xinner1 - 1)
  if(xcoord .lt. xin) then
	xinner = xinner1
	xs = 1 + xcoord*xinner
  else
	xinner = xinner1 - 1
	xs = 1 + xin*xinner1 + (xcoord - xin) * xinner
  endif
  xouter = xinner 
  xe = xs + xouter - 1	
!!  
  yinner1 = (Ny - 1)/ dimsCart(2) + 1
  yin     =  Ny - dimsCart(2)*(yinner1 - 1)
  if(ycoord .lt. yin) then
	yinner = yinner1
	ys = 1 + ycoord*yinner
  else
	yinner = yinner1 - 1
	ys = 1 + yin*yinner1 + (ycoord - yin) * yinner
  endif
  youter = yinner 
  ye = ys + youter - 1	

  zs = 1
  ze = Nz

  allocate(A(zs:ze,ys:ye,xs:xe))
  localSize = sizeof(A)/(1024*1024)


#if defined(DEBUG)
 do i=0,nprocs-1
  if(rank.eq.i) then
  write(*,*) 'PE = ', rank,' xs = ', xs, ' xe = ', xe 	
  write(*,*) 'PE = ', rank,' ys = ', ys, ' ye = ', ye 
  write(*,*) 'PE = ', rank,' zs = ', zs, ' ze = ', ze	
  write(*,*) 'Local Size[MB] = ', localSize	
  write(*,*)
  endif
  call MPI_BARRIER(MPI_COMM_WORLD,ierror)
enddo
#endif

  do i = xs, xe
   do j = ys, ye
    do k = zs, ze
     A(k,j,i) =  k + Nz*(Ny*(i-1) + j-1)
    enddo
   enddo
  enddo

  gsize(1) = Nz
  gsize(2) = Ny
  gsize(3) = Nx
  lsize(1) = ze-zs+1
  lsize(2) = ye-ys+1
  lsize(3) = xe-xs+1
  starts(1)= zs-1
  starts(2)= ys-1
  starts(3)= xs-1

  call MPI_TYPE_CREATE_SUBARRAY(3, gsize, lsize, starts, MPI_ORDER_FORTRAN, MPI_DOUBLE_PRECISION, subarray, ierror)
  call abort_io(ierror,"ERROR in MPI_TYPE_CREATE_SUBARRAY")
  call MPI_TYPE_COMMIT(subarray, ierror)
  call abort_io(ierror,"ERROR in MPI_TYPE_COMMIT")
  call MPI_FILE_SET_VIEW(mpiiofile%id, 0, MPI_DOUBLE_PRECISION, subarray, "native", MPI_INFO_NULL, ierror)
  call abort_io(ierror,"ERROR in MPI_FILE_SET_VIEW")

call MPI_Reduce(localSize, localSizeMAX, 1, MPI_INTEGER, MPI_MAX, 0, MPI_COMM_WORLD, ierror)
call MPI_Reduce(localSize, localSizeMIN, 1, MPI_INTEGER, MPI_MIN, 0, MPI_COMM_WORLD, ierror)
call MPI_Bcast(localSizeMAX, 1, MPI_INTEGER, 0,MPI_COMM_WORLD,ierror)

if(master()) then
   write(*,'(A,3I10)'), " Local Dim A(z,y,x) = ", ze-zs+1,ye-ys+1, xe-xs+1
   write(*,*), "Local size MAX [MB] = ", localSizeMAX
   write(*,*), "Local size MIN [MB] = ", localSizeMIN
endif

if(localSizeMAX .lt. LIMIT_2G) then
  call MPI_BARRIER(MPI_COMM_WORLD,ierror)
  write_time = takeTime()
  call MPI_FILE_WRITE_ALL(mpiiofile%id, A, lsize(1)*lsize(2)*lsize(3), MPI_DOUBLE_PRECISION, status, ierror)
  call abort_io(ierror,"ERROR in MPI_FILE_WRITE_ALL")
  call MPI_BARRIER(MPI_COMM_WORLD,ierror)
  write_time = takeTime() - write_time
else
  if(master()) then
	write(*,*), "** The Local Array of rank ", rank, "has dimension: ",localSize
        write(*,*), "** which is larger than ",LIMIT_2G
  	write(*,*), "** Test the 3D array in a 2D cartesian grid: FAILED"
  endif
endif
 
  call MPI_TYPE_FREE(subarray,ierror) 
  call MPI_COMM_FREE(commCart, ierror) 
  deallocate(A)

end subroutine start_test_2dcart_3darraympiio

subroutine start_test_3dcart_3darraympiio(Nx, Ny,Nz, mpiiofile)
  use mpivar
  use mytiming

  implicit none
  integer(kind=ik), intent(in) :: Nx, Ny, Nz
  type(datafileMPI)      :: mpiiofile
  real(kind=rk), allocatable, dimension(:,:,:)   :: A

  integer(kind=ik) :: xs, xe
  integer(kind=ik) :: ys, ye
  integer(kind=ik) :: zs, ze
  integer :: localSize, localSizeMAX, localSizeMIN  !in MB
  integer(kind=ik), parameter :: LIMIT_2G = (1024)*2 !in MB

  integer :: commCart			!Cartesian communicator
  integer(kind=ik) :: i,j,k
  integer(kind=ik) :: xinner1, yinner1, zinner1
  integer, parameter  :: cartDim = 3
  integer :: dimsCart(3), coords(3)		!number of nodes in each dimension
  integer(kind=ik) :: xcoord, ycoord, zcoord	!Local coordinates
  integer(kind=ik) :: xinner, xouter, xin
  integer(kind=ik) :: yinner, youter, yin
  integer(kind=ik) :: zinner, zouter, zin
  logical :: periods(3), reorder	!Periods of the cartesian grid and reorder

  integer :: subarray, status(MPI_STATUS_SIZE)
  integer :: gsize(3), lsize(3), starts(3)
 
  methodMPI = 6

  periods(1)  = .false.
  periods(2)  = .false.
  periods(3)  = .false.
  reorder     = .false.
  
  
  call MPI_DIMS_CREATE(nprocs, cartDim, dimsCart, ierror)
  call MPI_CART_CREATE(MPI_COMM_WORLD, cartDim, dimsCart, periods, reorder, commCart, ierror)
  call MPI_COMM_RANK(commCart, rank, ierror)
  call MPI_CART_COORDS(commCart, rank, cartDim ,coords, ierror)

  xcoord = coords(1)
  ycoord = coords(2)
  zcoord = coords(3)

 if(master()) then
  write(*,*), "Test the 3D array in a 3D cartesian grid"
  write(*,*), "Total Number of procs:   ",nprocs
  write(*,*), "Number of procs along x: ",dimsCart(1)
  write(*,*), "Number of procs along y: ",dimsCart(2)
  write(*,*), "Number of procs along z: ",dimsCart(3)
  write(*,'(A,3I10)'), " Array dimensions:",Nx,Ny,Nz
 endif

  xinner1 = (Nx - 1)/ dimsCart(1) + 1
  xin     =  Nx - dimsCart(1)*(xinner1 - 1)
  if(xcoord .lt. xin) then
	xinner = xinner1
	xs = 1 + xcoord*xinner
  else
	xinner = xinner1 - 1
	xs = 1 + xin*xinner1 + (xcoord - xin) * xinner
  endif
  xouter = xinner 
  xe = xs + xouter - 1	
!!  
  yinner1 = (Ny - 1)/ dimsCart(2) + 1
  yin     =  Ny - dimsCart(2)*(yinner1 - 1)
  if(ycoord .lt. yin) then
	yinner = yinner1
	ys = 1 + ycoord*yinner
  else
	yinner = yinner1 - 1
	ys = 1 + yin*yinner1 + (ycoord - yin) * yinner
  endif
  youter = yinner 
  ye = ys + youter - 1	
!!
  zinner1 = (Nz - 1)/ dimsCart(3) + 1
  zin     =  Nz - dimsCart(3)*(zinner1 - 1)
  if(zcoord .lt. zin) then
	zinner = zinner1
	zs = 1 + zcoord*zinner
  else
	zinner = zinner1 - 1
	zs = 1 + zin*zinner1 + (zcoord - zin) * zinner
  endif
  zouter = zinner 
  ze = zs + zouter - 1
	
  allocate(A(zs:ze,ys:ye,xs:xe))
  localSize = sizeof(A)/(1024*1024)


#if defined(DEBUG)
 do i=0,nprocs-1
  if(rank.eq.i) then
  write(*,*) 'PE = ', rank,' xs = ', xs, ' xe = ', xe 	
  write(*,*) 'PE = ', rank,' ys = ', ys, ' ye = ', ye 
  write(*,*) 'PE = ', rank,' zs = ', zs, ' ze = ', ze	
  write(*,*) 'Local Size[MB] = ', localSize	
  write(*,*)
  endif
  call MPI_BARRIER(MPI_COMM_WORLD,ierror)
enddo
#endif

  do i = xs, xe
   do j = ys, ye
    do k = zs, ze
     A(k,j,i) =  k + Nz*(Ny*(i-1) + j-1)
    enddo
   enddo
  enddo

  gsize(1) = Nz
  gsize(2) = Ny
  gsize(3) = Nx
  lsize(1) = ze-zs+1
  lsize(2) = ye-ys+1
  lsize(3) = xe-xs+1
  starts(1)= zs-1
  starts(2)= ys-1
  starts(3)= xs-1

  call MPI_TYPE_CREATE_SUBARRAY(3, gsize, lsize, starts, MPI_ORDER_FORTRAN, MPI_DOUBLE_PRECISION, subarray, ierror)
  call abort_io(ierror,"ERROR in MPI_TYPE_CREATE_SUBARRAY")
  call MPI_TYPE_COMMIT(subarray, ierror)
  call abort_io(ierror,"ERROR in MPI_TYPE_COMMIT")
  call MPI_FILE_SET_VIEW(mpiiofile%id, 0, MPI_DOUBLE_PRECISION, subarray, "native", MPI_INFO_NULL, ierror)
  call abort_io(ierror,"ERROR in MPI_FILE_SET_VIEW")

call MPI_Reduce(localSize, localSizeMAX, 1, MPI_INTEGER, MPI_MAX, 0, MPI_COMM_WORLD, ierror)
call MPI_Reduce(localSize, localSizeMIN, 1, MPI_INTEGER, MPI_MIN, 0, MPI_COMM_WORLD, ierror)
call MPI_Bcast(localSizeMAX, 1, MPI_INTEGER, 0,MPI_COMM_WORLD,ierror)

if(master()) then
   write(*,'(A,3I10)'), " Local Dim A(z,y,x) = ", ze-zs+1,ye-ys+1, xe-xs+1
   write(*,*), "Local size MAX [MB] = ", localSizeMAX
   write(*,*), "Local size MIN [MB] = ", localSizeMIN
endif

if(localSizeMAX .lt. LIMIT_2G) then
  call MPI_BARRIER(MPI_COMM_WORLD,ierror)
  write_time = takeTime()
  call MPI_FILE_WRITE_ALL(mpiiofile%id, A, lsize(1)*lsize(2)*lsize(3), MPI_DOUBLE_PRECISION, status, ierror)
  call abort_io(ierror,"ERROR in MPI_FILE_WRITE_ALL")
  call MPI_BARRIER(MPI_COMM_WORLD,ierror)
  write_time = takeTime() - write_time
else
  if(master()) then
	write(*,*), "** The Local Array of rank ", rank, "has dimension: ",localSize
        write(*,*), "** which is larger than ",LIMIT_2G
  	write(*,*), "** Test the 3D array in a 3D cartesian grid: FAILED"
  endif
endif
 
  call MPI_TYPE_FREE(subarray,ierror) 
  call MPI_COMM_FREE(commCart, ierror) 
  deallocate(A)

end subroutine start_test_3dcart_3darraympiio

end module mod_mpiio
