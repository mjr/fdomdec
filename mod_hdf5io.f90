!==================================================================
! Collection of driver routines for writing and reading HDF5 output
!
! author: Fabio Baruffa (RZG) 
!==================================================================
!TODO Modify the functions write_lc such that does not call the function
!to get the limit of the grid dimension (like linmod code) and since
!the xs,xe,...are public but they should be private memebers
module mod_hdf5io

  use precision
  use hdf5
  use my_mpi
  
  private 
 
  type, private :: datafileHDF5
      integer(kind=hid_t) :: id
      integer(kind=hid_t) :: current_group
      character(LEN=23)   :: name     !name = base+suffix
      character(LEN=20)   :: base
      character(LEN=3)    :: suffix
  end type
  
  type, private :: hdf5ioinit
  !== private fields ==
    integer, private :: mpi_rank                ! MPI rank
    integer, private :: mpi_proc		! MPI proc	
    integer, private :: mpi_comm                ! MPI comm
    integer, private :: mpi_info                ! MPI info
   contains
  !== private methods ==
    procedure, private :: io_init      ! Inizialize the hdf5 I/O
    procedure, private :: io_uninit    ! Finilize the hdf5 I/O
  end type hdf5ioinit
  
  
  type, public :: hdf5iofile
    type(datafileHDF5), private :: h5datafile
    contains
      private
      
      !== public methods == 
      procedure, public :: initialize   ! Inizialize the hdf5 I/O
      procedure, public :: finalize     ! clean up the hdf5 I/O
      procedure, public :: create_file  ! Create file with name
      procedure, public :: close_file   ! Close file with name
      procedure, public :: open_file    ! Open an hdf5 file
      procedure, public :: create_group ! Create the group for the hdf5 dataset
      procedure, public :: close_group  ! Close the group for the hdf5 dataset
      
 !== private methods ==
      procedure, private :: create_file_hdf5	! Create the hdf5 file
      procedure, private :: open_file_hdf5
      procedure, private :: close_file_hdf5
      procedure, private :: write_array_3d_lc
      procedure, private :: write_array_3d_gl
      procedure, private :: write_array_4d_lc
      procedure, private :: write_array_4d_gl
      procedure, private :: write_array_1d_gl
      procedure, private :: write_real_gl
      procedure, private :: write_int_gl
      procedure, private :: write_logical_gl
      procedure, private :: write_string_gl
      
      generic, public :: write_lc => write_array_3d_lc, write_array_4d_lc
      generic, public :: write_gl => write_array_3d_gl, write_array_4d_gl, &
				     write_array_1d_gl, write_real_gl, &
 				     write_int_gl, write_logical_gl, &
				     write_string_gl
      
  end type hdf5iofile

  type(hdf5ioinit), pointer, public :: phdf5 => null()
  type(hdf5iofile), public :: h5file

  !--------------------------------------------------------

 contains
 !-----------------------------------------------------------------
  subroutine finalize(this)
  
    implicit none
    class(hdf5iofile), intent(inout) :: this 
    integer :: ierr
    
    call phdf5%io_uninit()
       
  end subroutine finalize
  !-----------------------------------------------------------------
  subroutine initialize(this)
  
    implicit none
    class(hdf5iofile), intent(inout) :: this 
    integer :: ierr
    
     if (.not. associated(phdf5)) then 
      allocate(phdf5)
      call phdf5%io_init(ierr)
     endif
            
  end subroutine initialize
 !----------------------------------------------------------------- 
!> Initialize the hdf5 environment
  subroutine io_init(this,ierr)

    implicit none
    class(hdf5ioinit) :: this 
    integer, intent(out) :: ierr
    integer :: ioerror
      
      ierr = 0 
#if defined(MPICODE)
  !In case we want to set the IO info, like romio, collective, etc...
  !This function has to be implemented in the module of mpi
  !call set_io_info
      call MPI_INFO_CREATE(comm%info, ierr)
      
      phdf5%mpi_comm=comm%mpicomm
      phdf5%mpi_info=comm%info
      phdf5%mpi_rank=comm%myrank
      phdf5%mpi_proc=comm%numproc
  
  !This function has to be implemented in the module of mpi 
  !call get_io_info
    
#else
      phdf5%mpi_comm=0
      phdf5%mpi_info=0
      phdf5%mpi_rank=0
      phdf5%mpi_proc=1
#endif
    ! start hdf5
      call h5open_f(ioerror)
      call comm%abort_io(ioerror, "ERROR in h5open_f in io_init")
    
  end subroutine io_init
!-----------------------------------------------------------------
  subroutine io_uninit(this)
  
    implicit none
    class(hdf5ioinit) :: this 
    integer :: ioerror
    
    ! stop hdf5
    call h5close_f(ioerror)
    call comm%abort_io(ioerror, "ERROR in h5close_f in io_uninit")

  end subroutine io_uninit
!-----------------------------------------------------------------
  subroutine create_file(this,basename)
  
    implicit none
    class(hdf5iofile) :: this
    character(*), intent(in) :: basename	!name of the file
    character(3), parameter  :: suffix = '.h5'
    
    call this%create_file_hdf5(basename,suffix)
    
  end subroutine create_file
  !-----------------------------------------------------------------
  subroutine close_file(this,basename)
  
    implicit none
    class(hdf5iofile) :: this
    character(*), intent(in) :: basename	!name of the file
    character(3), parameter  :: suffix = '.h5'
    
    call this%close_file_hdf5(basename,suffix)
    
  end subroutine close_file
!-----------------------------------------------------------------
  subroutine create_file_hdf5(this,basename,suffix)
    use my_mpi
    implicit none
    
    class(hdf5iofile) :: this
    character(*), intent(in) :: basename
    character(*), intent(in) :: suffix
    integer :: ioerror
    integer(kind=hid_t) :: plist_id, file_id 
    integer :: mpi_info, mpi_comm    
    
    mpi_comm = phdf5%mpi_comm
    mpi_info = phdf5%mpi_info
    
    this%h5datafile%base   = trim(basename)
    this%h5datafile%suffix = trim(suffix)
    this%h5datafile%name = trim(trim(this%h5datafile%base)//trim(this%h5datafile%suffix))
    
    ! Set up file access property list with parallel I/O access
    call h5pcreate_f(H5P_FILE_ACCESS_F, plist_id, ioerror)
    call comm%abort_io(ioerror,"ERROR in h5pcreate_f in create_file_hdf5")

#if defined(MPICODE)
    call h5pset_fapl_mpio_f(plist_id,mpi_comm, mpi_info, ioerror)
    call comm%abort_io(ioerror,"ERROR in h5pset_fapl_mpio_f in create_file_hdf5")
#endif
    ! create a new file
    call h5fcreate_f(this%h5datafile%name, H5F_ACC_TRUNC_F, file_id, ioerror, access_prp=plist_id)
    call comm%abort_io(ioerror,"ERROR in h5fcreate_f in create_file_hdf5")

    ! close the plist again, not needed anymore
    call h5pclose_f(plist_id, ioerror)
    call comm%abort_io(ioerror,"ERROR in h5pclose_f in create_file_hdf5")

    this%h5datafile%id = file_id
    this%h5datafile%current_group = 0
     
    if (comm%isRoot()) write(*,*) " "
    if (comm%isRoot()) write(*,*) "Created the HDF5 file with name: ", this%h5datafile%name

  end subroutine create_file_hdf5
 !-----------------------------------------------------------------
!> Close an opened datafile
  subroutine close_file_hdf5(this,basename,suffix)
    
    implicit none
    class(hdf5iofile) :: this
    character(*), intent(in) :: basename
    character(*), intent(in) :: suffix
    character(23)            :: name
    integer :: ioerror

    name = trim(trim(basename)//trim(suffix))

    if(name == this%h5datafile%name) then
      call h5fclose_f(this%h5datafile%id, ioerror)
      call comm%abort_io(ioerror,"ERROR in h5fclose_f close_file_hdf5")
      if (comm%isRoot()) write(*,*), "Closed the file: ",this%h5datafile%name

    else
      if (comm%isRoot()) write(*,*), "Cannot Close the file: ",name, ". Incompatible file names!"
    endif

  end subroutine close_file_hdf5
!-----------------------------------------------------------------
!> Create the group for hdf5 dataset
  subroutine create_group(this,group_name)
  
    implicit none
    class(hdf5iofile) :: this
    character(*), intent(in) :: group_name
    integer :: ioerror
    
    call h5gcreate_f(this%h5datafile%id,group_name,this%h5datafile%current_group, ioerror)
    call comm%abort_io(ioerror,"ERROR in h5gcreate_f create_group")
    
  end subroutine create_group
!-----------------------------------------------------------------
!> Close the group for hdf5 dataset
  subroutine close_group(this,group_name)
  
    implicit none
    class(hdf5iofile) :: this
    character(*), intent(in) :: group_name
    integer :: ioerror
    
    call h5gclose_f(this%h5datafile%current_group, ioerror)
    call comm%abort_io(ioerror,"ERROR in h5gclose_f close_group")
    
  end subroutine close_group

!-----------------------------------------------------------------
!> Write a 3D distributed array in real or double precision
 subroutine write_array_3d_lc(this, data, dname, dimsfin, offsetin)
    use mod_mpidomain
    
    implicit none
    class(hdf5iofile) :: this
    integer, parameter  :: rank = 3
    real(kind=PREC_F90), intent(in), target :: data(domain%get_xs():domain%get_xe(), &
					            domain%get_ys():domain%get_ye(), &
					            domain%get_zs():domain%get_ze())
!     real(kind=PREC_F90), intent(in), target :: data(domain%xs:domain%xe, &
! 					            domain%ys:domain%ye, &
! 					            domain%zs:domain%ze)


    real(kind=PREC_F90), pointer :: outdata(:,:,:)					      
    character(*), intent(in)     :: dname
    integer(kind=hsize_t), optional , dimension(rank) :: dimsfin, offsetin
    integer(kind=hsize_t), dimension(rank)  :: dimsf, offset
    integer(kind=hsize_t), dimension(rank) :: dims
    integer :: xlft, xrgt, ylft, yrgt, zlft, zrgt
    integer :: PREC_HDF
    integer :: error !, istat

    integer(kind=hid_t)         :: filespace, memspace, dset_id, plist_id, group_id

#if defined DOUBLE_PREC
    PREC_HDF = H5T_NATIVE_DOUBLE
#else
    PREC_HDF = H5T_NATIVE_REAL  
#endif

    xlft = domain%get_xlft()
    xrgt = domain%get_xrgt()
    ylft = domain%get_ylft()
    yrgt = domain%get_yrgt()
    zlft = domain%get_zlft()
    zrgt = domain%get_zrgt()
    
    if(present(dimsfin) .and.  present(offsetin) ) then
      dimsf(:)  = dimsfin(:)
      offset(:) = offsetin(:)
    else
      dimsf(1)  = domain%get_gx()
      dimsf(2)  = domain%get_gy()
      dimsf(3)  = domain%get_gz()
      offset(1) = xlft - 1 
      offset(2) = ylft - 1
      offset(3) = zlft - 1
    endif
  
    outdata => data(xlft:xrgt, ylft:yrgt, zlft:zrgt )

    dims = shape(outdata)

    group_id = this%h5datafile%current_group
    
    call h5screate_simple_f(rank, dimsf, filespace, error)
    call comm%abort_io(error,"ERROR in h5screate_simple_f in write_array_3d_lc")

    ! Create the dataset
    call h5dcreate_f(group_id, dname, PREC_HDF, filespace, dset_id, error)
    call comm%abort_io(error,"ERROR in h5dcreate_f in write_array_3d_lc")

    ! Release the dataspace
    call h5sclose_f(filespace, error)
    call comm%abort_io(error,"ERROR in h5sclose_f in write_array_3d_lc")

    ! define our dataset in memory
    call h5screate_simple_f(rank, dims, memspace, error)
    call comm%abort_io(error,"ERROR in h5screate_simple_f in write_array_3d_lc")

    ! Select hyperslab in the file.
    call h5dget_space_f(dset_id, filespace, error)
    call comm%abort_io(error,"ERROR in h5dget_space_f in write_array_3d_lc")

    call h5sselect_hyperslab_f(filespace, H5S_SELECT_SET_F, offset, dims, error)
    call comm%abort_io(error,"ERROR in h5sselect_hyperslab_f in write_array_3d_lc")

    ! Create property list for collective dataset write.
    call h5pcreate_f(H5P_DATASET_XFER_F, plist_id, error)
    call comm%abort_io(error,"ERROR in h5pcreate_f in write_array_3d_lc")

#if defined(MPICODE)
    call h5pset_dxpl_mpio_f(plist_id, H5FD_MPIO_COLLECTIVE_F, error)
    call comm%abort_io(error,"ERROR in h5pset_dxpl_mpio_f in write_array_3d_lc")
#endif

!     Do the write
    call h5dwrite_f(dset_id, PREC_HDF, outdata, dims, error, &
          file_space_id = filespace, mem_space_id = memspace, xfer_prp = plist_id)
    call comm%abort_io(error,"ERROR in h5dwrite_f in write_array_3d_lc")

    ! Release allocated objects
    !> \todo check for memory leaks
    call h5dclose_f(dset_id, error)
    call comm%abort_io(error,"ERROR in h5dclose_f in write_array_3d_lc")

    call h5sclose_f(filespace, error)
    call comm%abort_io(error,"ERROR in h5sclose_f in write_array_3d_lc")

    call h5sclose_f(memspace, error)
    call comm%abort_io(error,"ERROR in h5sclose_f in write_array_3d_lc")

    call h5pclose_f(plist_id, error)
    call comm%abort_io(error,"ERROR in h5pclose_f in write_array_3d_lc")
    
  end subroutine write_array_3d_lc

!-----------------------------------------------------------------
!> Write a 4D distributed array in real or double precision
 subroutine write_array_4d_lc(this, data, ws, we, dname, dimsfin, offsetin)
    use mod_mpidomain
    
    implicit none
    class(hdf5iofile) :: this
    integer, parameter  :: rank = 3
    integer, intent(in) :: ws, we
    real(kind=PREC_F90), intent(in), target :: data(ws:we,			     &
						    domain%get_xs():domain%get_xe(), &
					            domain%get_ys():domain%get_ye(), &
					            domain%get_zs():domain%get_ze())
!     real(kind=PREC_F90), intent(in), target :: data(ws:we,			     &
! 						    domain%xs:domain%xe, &
! 					            domain%ys:domain%ye, &
! 					            domain%zs:domain%ze)
    real(kind=PREC_F90), pointer :: outdata(:,:,:,:)
    character(*), intent(in)      :: dname
    character(len=20) :: string_name
    character(len=20) :: suffix
    integer :: string_len
    integer :: k
    integer(kind=hsize_t), optional , dimension(rank) :: dimsfin, offsetin
    integer(kind=hsize_t), dimension(rank)  :: dimsf, offset
    integer(kind=hsize_t), dimension(rank) :: dims
    integer :: xlft, xrgt, ylft, yrgt, zlft, zrgt
    integer :: PREC_HDF
    integer :: error !, istat

    integer(kind=hid_t)         :: filespace, memspace, dset_id, plist_id, group_id

#if defined DOUBLE_PREC
    PREC_HDF = H5T_NATIVE_DOUBLE
#else
    PREC_HDF = H5T_NATIVE_REAL  
#endif

    xlft = domain%get_xlft()
    xrgt = domain%get_xrgt()
    ylft = domain%get_ylft()
    yrgt = domain%get_yrgt()
    zlft = domain%get_zlft()
    zrgt = domain%get_zrgt()
    
    if(present(dimsfin) .and.  present(offsetin) ) then
      dimsf(:)  = dimsfin(:)
      offset(:) = offsetin(:)
    else
      dimsf(1)  = domain%get_gx()
      dimsf(2)  = domain%get_gy()
      dimsf(3)  = domain%get_gz()
      offset(1) = xlft - 1 
      offset(2) = ylft - 1
      offset(3) = zlft - 1
    endif
    
    
    do k=ws,we
      write (suffix, '(I2.2)') k
      suffix = adjustl(suffix)
      string_name = trim(dname)//'_'//trim(suffix)

      outdata => data(k:k, xlft:xrgt, ylft:yrgt, zlft:zrgt )

      dims = shape(outdata(1,:,:,:))

      group_id = this%h5datafile%current_group
    
      call h5screate_simple_f(rank, dimsf, filespace, error)
      call comm%abort_io(error,"ERROR in h5screate_simple_f in write_array_4d_lc")

      ! Create the dataset
      call h5dcreate_f(group_id, string_name, PREC_HDF, filespace, dset_id, error)
      call comm%abort_io(error,"ERROR in h5dcreate_f in write_array_4d_lc")

      ! Release the dataspace
      call h5sclose_f(filespace, error)
      call comm%abort_io(error,"ERROR in h5sclose_f in write_array_4d_lc")

      ! define our dataset in memory
      call h5screate_simple_f(rank, dims, memspace, error)
      call comm%abort_io(error,"ERROR in h5screate_simple_f in write_array_4d_lc")

      ! Select hyperslab in the file.
      call h5dget_space_f(dset_id, filespace, error)
      call comm%abort_io(error,"ERROR in h5dget_space_f in write_array_4d_lc")

      call h5sselect_hyperslab_f(filespace, H5S_SELECT_SET_F, offset, dims, error)
      call comm%abort_io(error,"ERROR in h5sselect_hyperslab_f in write_array_4d_lc")

      ! Create property list for collective dataset write.
      call h5pcreate_f(H5P_DATASET_XFER_F, plist_id, error)
      call comm%abort_io(error,"ERROR in h5pcreate_f in write_array_4d_lc")

#if defined(MPICODE)
      call h5pset_dxpl_mpio_f(plist_id, H5FD_MPIO_COLLECTIVE_F, error)
      call comm%abort_io(error,"ERROR in h5pset_dxpl_mpio_f in write_array_4d_lc")
#endif

!     Do the write
      call h5dwrite_f(dset_id, PREC_HDF, outdata, dims, error, &
	    file_space_id = filespace, mem_space_id = memspace, xfer_prp = plist_id)
      call comm%abort_io(error,"ERROR in h5dwrite_f in write_array_4d_lc")

    ! Release allocated objects
    !> \todo check for memory leaks
      call h5dclose_f(dset_id, error)
      call comm%abort_io(error,"ERROR in h5dclose_f in write_array_4d_lc")

      call h5sclose_f(filespace, error)
      call comm%abort_io(error,"ERROR in h5sclose_f in write_array_4d_lc")

      call h5sclose_f(memspace, error)
      call comm%abort_io(error,"ERROR in h5sclose_f in write_array_4d_lc")

      call h5pclose_f(plist_id, error)
      call comm%abort_io(error,"ERROR in h5pclose_f in write_array_4d_lc")
    
    enddo
    
    
  end subroutine write_array_4d_lc
!-----------------------------------------------------------------
!> Write a 3D global array in real or double precision
 subroutine write_array_3d_gl(this, data, dname)
    use mod_mpidomain
    
    implicit none
    class(hdf5iofile) :: this
    integer, parameter  :: rank = 3
    real(kind=PREC_F90), intent(in), target :: data(:,:,:)
    character(*), intent(in)     :: dname
    integer(kind=hsize_t), dimension(rank) :: dims
    integer :: PREC_HDF
    integer :: error !, istat

    integer(kind=hid_t)         :: filespace, memspace, dset_id, plist_id, group_id

#if defined DOUBLE_PREC
    PREC_HDF = H5T_NATIVE_DOUBLE
#else
    PREC_HDF = H5T_NATIVE_REAL  
#endif

    dims = shape(data)

    group_id = this%h5datafile%current_group
    
    call h5screate_simple_f(rank, dims, filespace, error)
    call comm%abort_io(error,"ERROR in h5screate_simple_f in write_array_3d_gl")

    ! Create the dataset
    call h5dcreate_f(group_id, dname, PREC_HDF, filespace, dset_id, error)
    call comm%abort_io(error,"ERROR in h5dcreate_f in write_array_3d_gl")

    ! Release the dataspace
    call h5sclose_f(filespace, error)
    call comm%abort_io(error,"ERROR in h5sclose_f in write_array_3d_gl")

    ! define our dataset in memory
    call h5screate_simple_f(rank, dims, memspace, error)
    call comm%abort_io(error,"ERROR in h5screate_simple_f in write_array_3d_gl")

    call h5dget_space_f(dset_id, filespace, error)
    call comm%abort_io(error,"ERROR in h5dget_space_f in write_array_3d_gl")

    call h5sselect_all_f (filespace,error)
    call comm%abort_io(error,"ERROR in h5sselect_all_f in write_array_3d_gl")

    ! Create property list for collective dataset write.
    call h5pcreate_f(H5P_DATASET_XFER_F, plist_id, error)
    call comm%abort_io(error,"ERROR in h5pcreate_f in write_array_3d_gl")

#if defined(MPICODE)
    call h5pset_dxpl_mpio_f(plist_id, H5FD_MPIO_COLLECTIVE_F, error)
    call comm%abort_io(error,"ERROR in h5pset_dxpl_mpio_f in write_array_3d_gl")
#endif

!     Do the write
    call h5dwrite_f(dset_id, PREC_HDF, data, dims, error, &
          file_space_id = filespace, mem_space_id = memspace, xfer_prp = plist_id)
    call comm%abort_io(error,"ERROR in h5dwrite_f in write_array_3d_gl")

    ! Release allocated objects
    !> \todo check for memory leaks
    call h5dclose_f(dset_id, error)
    call comm%abort_io(error,"ERROR in h5dclose_f in write_array_3d_gl")

    call h5sclose_f(filespace, error)
    call comm%abort_io(error,"ERROR in h5sclose_f in write_array_3d_gl")

    call h5sclose_f(memspace, error)
    call comm%abort_io(error,"ERROR in h5sclose_f in write_array_3d_gl")

    call h5pclose_f(plist_id, error)
    call comm%abort_io(error,"ERROR in h5pclose_f in write_array_3d_gl")
    
  end subroutine write_array_3d_gl

!-----------------------------------------------------------------
!> Write a 4D global array in real or double precision
 subroutine write_array_4d_gl(this, data, ws, we, dname)
    use mod_mpidomain
    
    implicit none
    class(hdf5iofile) :: this
    integer, parameter  :: rank = 3
    integer, intent(in) :: ws, we
    real(kind=PREC_F90), intent(in), target :: data(:,:,:,:)
    character(*), intent(in)      :: dname
    character(len=20) :: string_name
    character(len=20) :: suffix
    integer :: string_len
    integer :: k
    integer(kind=hsize_t), dimension(rank) :: dims
    integer :: PREC_HDF
    integer :: error !, istat

    integer(kind=hid_t)         :: filespace, memspace, dset_id, plist_id, group_id

#if defined DOUBLE_PREC
    PREC_HDF = H5T_NATIVE_DOUBLE
#else
    PREC_HDF = H5T_NATIVE_REAL  
#endif
    

    
    do k=ws,we
      write (suffix, '(I2.2)') k
      suffix = adjustl(suffix)
      string_name = trim(trim(dname)//'_'//trim(suffix))

      dims = shape(data(1,:,:,:))

      group_id = this%h5datafile%current_group
    
      call h5screate_simple_f(rank, dims, filespace, error)
      call comm%abort_io(error,"ERROR in h5screate_simple_f in write_f_array_4d")

      ! Create the dataset
      call h5dcreate_f(group_id, string_name, PREC_HDF, filespace, dset_id, error)
      call comm%abort_io(error,"ERROR in h5dcreate_f in write_f_array_4d")

      ! Release the dataspace
      call h5sclose_f(filespace, error)
      call comm%abort_io(error,"ERROR in h5sclose_f in write_f_array_4d")

      ! define our dataset in memory
      call h5screate_simple_f(rank, dims, memspace, error)
      call comm%abort_io(error,"ERROR in h5screate_simple_f in write_f_array_4d")

      call h5dget_space_f(dset_id, filespace, error)
      call comm%abort_io(error,"ERROR in h5dget_space_f in write_f_array_4d")

      call h5sselect_all_f (filespace,error)
      call comm%abort_io(error,"ERROR in h5sselect_all_f in write_f_array_4d")

      ! Create property list for collective dataset write.
      call h5pcreate_f(H5P_DATASET_XFER_F, plist_id, error)
      call comm%abort_io(error,"ERROR in h5pcreate_f in write_f_array_4d")

#if defined(MPICODE)
      call h5pset_dxpl_mpio_f(plist_id, H5FD_MPIO_COLLECTIVE_F, error)
      call comm%abort_io(error,"ERROR in h5pset_dxpl_mpio_f in write_f_array_4d")
#endif

!     Do the write
      call h5dwrite_f(dset_id, PREC_HDF, data, dims, error, &
	    file_space_id = filespace, mem_space_id = memspace, xfer_prp = plist_id)
      call comm%abort_io(error,"ERROR in h5dwrite_f in write_f_array_4d")

    ! Release allocated objects

      call h5dclose_f(dset_id, error)
      call comm%abort_io(error,"ERROR in h5dclose_f in write_f_array_4d")

      call h5sclose_f(filespace, error)
      call comm%abort_io(error,"ERROR in h5sclose_f in write_f_array_4d")

      call h5sclose_f(memspace, error)
      call comm%abort_io(error,"ERROR in h5sclose_f in write_f_array_4d")

      call h5pclose_f(plist_id, error)
      call comm%abort_io(error,"ERROR in h5pclose_f in write_f_array_4d")
    
    enddo
        
  end subroutine write_array_4d_gl
!-----------------------------------------------------------------
  subroutine open_file(this,basename)
    implicit none
    class(hdf5iofile) :: this
    character(*), intent(in) :: basename	!name of the file
    character(3), parameter  :: suffix = '.h5'
    
    call this%open_file_hdf5(basename,suffix)
    
  end subroutine
!-----------------------------------------------------------------
!   > Open an existing HDF5 input file, possibly MPI aware, for reading
  subroutine open_file_hdf5(this,basename,suffix)

    implicit none
    class(hdf5iofile) :: this
    character(*), intent(in) :: basename
    character(*), intent(in) :: suffix
    integer(HID_T) :: plist_id,file_id
    integer :: error

    this%h5datafile%id = 0
    this%h5datafile%current_group = 0
    
    this%h5datafile%base   = trim(basename)
    this%h5datafile%suffix = trim(suffix)
    this%h5datafile%name   = trim(this%h5datafile%base//this%h5datafile%suffix)
    
    ! Set up file access property list with parallel I/O access
    call h5pcreate_f(H5P_FILE_ACCESS_F, plist_id, error)
    call comm%abort_io(error,"ERROR in h5pcreate_f in open_file_hdf5")
#if defined(MPICODE) 
    call h5pset_fapl_mpio_f(plist_id,phdf5%mpi_comm, phdf5%mpi_info, error)
    call comm%abort_io(error,"ERROR in h5pset_fapl_mpio_f in open_file_hdf5")
#endif
    ! open a new file
    call h5fopen_f(this%h5datafile%name, H5F_ACC_RDONLY_F, this%h5datafile%id, error, access_prp=plist_id)
    call comm%abort_io(error,"ERROR in h5fopen_f in open_file_hdf5")

    ! close the plist again, not needed anymore
    call h5pclose_f(plist_id, error)
    call comm%abort_io(error,"ERROR in h5pclose_f in open_file_hdf5")

    if (comm%isRoot()) write(*,*), "Opened the file: ",this%h5datafile%name
        
  end subroutine open_file_hdf5
!-----------------------------------------------------------------
!> Write a 1D global array in real or double precision
  subroutine write_array_1d_gl(this, data, dname)
    implicit none
    class(hdf5iofile) :: this
    integer, parameter        :: rank = 1
    real(kind=PREC_F90), intent(in)    :: data(:)
    character(*), intent(in)     :: dname
    integer(kind=hsize_t), dimension(rank)  :: dims
    integer :: error !, istat
    integer(kind=hid_t)         :: filespace, memspace, dset_id, plist_id, group_id
    integer :: PREC_HDF
      
#if defined DOUBLE_PREC
    PREC_HDF = H5T_NATIVE_DOUBLE
#else
    PREC_HDF = H5T_NATIVE_REAL  
#endif
      
    dims = shape(data(:))
    
    group_id = this%h5datafile%current_group
    
    call h5screate_simple_f(rank, dims, filespace, error)
    call comm%abort_io(error,"ERROR in h5screate_simple_f in write_array_1d_gl")

    ! Create the dataset
    call h5dcreate_f(group_id, dname, PREC_HDF, filespace, dset_id, error)
    call comm%abort_io(error,"ERROR in h5dcreate_f in write_array_1d_gl")

    ! Release the dataspace
    call h5sclose_f(filespace, error)
    call comm%abort_io(error,"ERROR in h5sclose_f in write_array_1d_gl")

    ! define our dataset in memory
    call h5screate_simple_f(rank, dims, memspace, error)
    call comm%abort_io(error,"ERROR in h5screate_simple_f in write_array_1d_gl")

    call h5dget_space_f(dset_id, filespace, error)
    call comm%abort_io(error,"ERROR in h5dget_space_f in write_array_1d_gl")

    CALL h5sselect_all_f (filespace,error)
    call comm%abort_io(error,"ERROR in h5sselect_all_f in write_array_1d_gl")

    ! Create property list for collective dataset write
    CALL h5pcreate_f(H5P_DATASET_XFER_F, plist_id, error) 
    call comm%abort_io(error,"ERROR in h5pcreate_f in write_array_1d_gl")
#ifdef MPICODE 
    CALL h5pset_dxpl_mpio_f(plist_id, H5FD_MPIO_COLLECTIVE_F, error)
    call comm%abort_io(error,"ERROR in h5pset_dxpl_mpio_f in write_array_1d_gl")
#endif
    ! Do the write
    call h5dwrite_f(dset_id, PREC_HDF, data, dims, error, &
          file_space_id = filespace, mem_space_id = memspace, xfer_prp = plist_id)
    call comm%abort_io(error,"ERROR in h5dwrite_f in write_array_1d_gl")

    ! Release allocated objects
    !> \todo check for memory leaks
    call h5dclose_f(dset_id, error)
    call comm%abort_io(error,"ERROR in h5dclose_f in write_array_1d_gl")

    call h5sclose_f(filespace, error)
    call comm%abort_io(error,"ERROR in h5sclose_f in write_array_1d_gl")

    call h5sclose_f(memspace, error)
    call comm%abort_io(error,"ERROR in h5sclose_f in write_array_1d_gl")

    call h5pclose_f(plist_id, error)
    call comm%abort_io(error,"ERROR in h5pclose_f in write_array_1d_gl")

  end subroutine write_array_1d_gl
! 
!   !> Write a global real value
  subroutine write_real_gl(this,data,dname)
    implicit none
    class(hdf5iofile) :: this
    real(kind=PREC_F90), intent(in)  :: data
    character(*), intent(in) :: dname
    integer :: error
    integer(HID_T) :: attr_id,group_id,dspace_id
    integer :: PREC_HDF
      
#if defined DOUBLE_PREC
    PREC_HDF = H5T_NATIVE_DOUBLE
#else
    PREC_HDF = H5T_NATIVE_REAL  
#endif

    group_id = this%h5datafile%current_group

    ! Create scalar dataspace
    call h5screate_f(H5S_SCALAR_F, dspace_id, error)
    call comm%abort_io(error,"ERROR in h5screate_f in write_real_gl")
    
    !Create the attribute
    call h5acreate_f(group_id, dname, PREC_HDF, dspace_id, attr_id, error)
    call comm%abort_io(error,"ERROR in h5acreate_f in write_real_gl")
    
    call h5awrite_f(attr_id, PREC_HDF, data, (/1_hsize_t/), error)
    call comm%abort_io(error,"ERROR in h5awrite_f in write_real_gl")
 
    call h5aclose_f(attr_id, error)
    call comm%abort_io(error,"ERROR in h5aclose_f in write_real_gl")

    call h5sclose_f(dspace_id, error)
    call comm%abort_io(error,"ERROR in h5sclose_f in write_real_gl")

  end subroutine write_real_gl
 
!   !> Write a global int value
  subroutine write_int_gl(this,data,dname)
    implicit none
    class(hdf5iofile) :: this
    integer, intent(in)  :: data
    character(*), intent(in) :: dname
    integer :: error
    integer(HID_T) :: attr_id,group_id,dspace_id

    group_id = this%h5datafile%current_group

    ! Create scalar dataspace
    call h5screate_f(H5S_SCALAR_F, dspace_id, error)
    call comm%abort_io(error,"ERROR in h5screate_f in write_int_gl")
    
    !Create the attribute
    call h5acreate_f(group_id, dname, H5T_NATIVE_INTEGER, dspace_id, attr_id, error)
    call comm%abort_io(error,"ERROR in h5acreate_f in write_int_gl")
    
    call h5awrite_f(attr_id, H5T_NATIVE_INTEGER, data, (/1_hsize_t/), error)
    call comm%abort_io(error,"ERROR in h5awrite_f in write_int_gl")
 
    call h5aclose_f(attr_id, error)
    call comm%abort_io(error,"ERROR in h5aclose_f in write_int_gl")

    call h5sclose_f(dspace_id, error)
    call comm%abort_io(error,"ERROR in h5sclose_f in write_int_gl")

  end subroutine write_int_gl
  
  !   !> Write a global logical value
  subroutine write_logical_gl(this,data,dname)
    implicit none
    class(hdf5iofile) :: this
    logical, intent(in)  :: data
    character(*), intent(in) :: dname
    integer :: error
    integer(HID_T) :: attr_id,group_id,dspace_id
    integer :: datal
    
    if(data) then
      datal=1
    else
      datal=0
    endif
    
    group_id = this%h5datafile%current_group

    ! Create scalar dataspace
    call h5screate_f(H5S_SCALAR_F, dspace_id, error)
    call comm%abort_io(error,"ERROR in h5screate_f in write_logical_gl")
    
    !Create the attribute
    call h5acreate_f(group_id, dname, H5T_NATIVE_INTEGER , dspace_id, attr_id, error)
    call comm%abort_io(error,"ERROR in h5acreate_f in write_logical_gl")
    
    call h5awrite_f(attr_id, H5T_NATIVE_INTEGER , datal, (/1_hsize_t/), error)
    call comm%abort_io(error,"ERROR in h5awrite_f in write_logical_gl")
 
    call h5aclose_f(attr_id, error)
    call comm%abort_io(error,"ERROR in h5aclose_f in write_logical_gl")

    call h5sclose_f(dspace_id, error)
    call comm%abort_io(error,"ERROR in h5sclose_f in write_logical_gl")

  end subroutine write_logical_gl
  
    !   !> Write a global string value
  subroutine write_string_gl(this,data,dname)
    implicit none
    class(hdf5iofile) :: this
    character(*), intent(in) :: data
    character(*), intent(in) :: dname
    integer :: error
    integer(HID_T) :: attr_id,group_id,dspace_id,dtype_id

    group_id = this%h5datafile%current_group

    ! Create scalar dataspace
    call h5screate_f(H5S_SCALAR_F, dspace_id, error)
    call comm%abort_io(error,"ERROR in h5screate_f in write_string_gl")
    
    ! Create the datatype
    call h5tcopy_f(H5T_NATIVE_CHARACTER, dtype_id, error)
    call comm%abort_io(error,"ERROR in h5tcopy_f in write_string_gl")

    call h5tset_size_f(dtype_id, int(len(trim(data)), kind=size_t), error)
    call comm%abort_io(error,"ERROR in h5tset_size_f in write_string_gl")

    !Create the attribute
    call h5acreate_f(group_id, dname,dtype_id , dspace_id, attr_id, error)
    call comm%abort_io(error,"ERROR in h5acreate_f in write_string_gl")
    
    call h5awrite_f(attr_id, dtype_id, data, (/1_hsize_t/), error)
    call comm%abort_io(error,"ERROR in h5awrite_f in write_string_gl")
    
    call h5aclose_f(attr_id, error)
    call comm%abort_io(error,"ERROR in h5aclose_f in write_string_gl")

    CALL h5sclose_f(dspace_id, error)
    call comm%abort_io(error,"ERROR in h5sclose_f in write_string_gl")
    
    call h5tclose_f(dtype_id, error)
    call comm%abort_io(error,"ERROR in h5tclose_f in write_string_gl")

  end subroutine write_string_gl
end module mod_hdf5io
